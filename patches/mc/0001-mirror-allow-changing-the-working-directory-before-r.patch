From 53026f6dbbfd086d8580f7cebd89da635bcb510b Mon Sep 17 00:00:00 2001
From: Martin Peres <martin.peres@mupuf.org>
Date: Thu, 2 Sep 2021 18:45:34 +0300
Subject: [PATCH] mirror: allow changing the working directory before running
 the cmd

This is a workaround for the
"MinIO Trims Empty Prefixes on Object Removal"[1] insanity that ends up
breaking podman volumes...

Consider the following:

mcli mirror --remove $src /my/dest/folder/

if /my/dest/folder/ is containing a file which not found in $src, then
the file will get deleted, along with all the folders until / if they
are empty.

[1] https://docs.min.io/minio/baremetal/reference/minio-cli/minio-mc/mc-mirror.html#id2
---
 cmd/mirror-main.go | 15 +++++++++++++++
 cmd/mirror-url.go  |  1 +
 2 files changed, 16 insertions(+)

diff --git a/cmd/mirror-main.go b/cmd/mirror-main.go
index 96b9abf3..303d2d41 100644
--- a/cmd/mirror-main.go
+++ b/cmd/mirror-main.go
@@ -28,6 +28,7 @@ import (
 	"strings"
 	"sync"
 	"time"
+	"os"
 
 	"github.com/fatih/color"
 	"github.com/minio/cli"
@@ -129,6 +130,10 @@ var (
 			Name:  "monitoring-address",
 			Usage: "if specified, a new prometheus endpoint will be created to report mirroring activity. (eg: localhost:8081)",
 		},
+		cli.StringFlag{
+			Name:  "working-directory, r",
+			Usage: "Change the working directory to the specified value before operating",
+		},
 	}
 )
 
@@ -920,6 +925,7 @@ func runMirror(ctx context.Context, srcURL, dstURL string, cli *cli.Context, enc
 		olderThan:             cli.String("older-than"),
 		newerThan:             cli.String("newer-than"),
 		storageClass:          cli.String("storage-class"),
+		workingDirectory:      cli.String("working-directory"),
 		userMetadata:          userMetadata,
 		encKeyDB:              encKeyDB,
 		activeActive:          isWatch,
@@ -1028,6 +1034,15 @@ func runMirror(ctx context.Context, srcURL, dstURL string, cli *cli.Context, enc
 
 // Main entry point for mirror command.
 func mainMirror(cliCtx *cli.Context) error {
+	workDir := cliCtx.String("working-directory")
+	if len(workDir) > 0 {
+		fmt.Printf("Switching the working directory to '%s'\n", workDir)
+		err := os.Chdir(workDir)
+		if err != nil {
+			fatalIf(probe.NewError(err), "Unable to change the work directory")
+		}
+	}
+
 	// Additional command specific theme customization.
 	console.SetColor("Mirror", color.New(color.FgGreen, color.Bold))
 
diff --git a/cmd/mirror-url.go b/cmd/mirror-url.go
index e35a2ed6..64c6cd84 100644
--- a/cmd/mirror-url.go
+++ b/cmd/mirror-url.go
@@ -223,6 +223,7 @@ type mirrorOptions struct {
 	md5, disableMultipart                 bool
 	olderThan, newerThan                  string
 	storageClass                          string
+	workingDirectory                      string
 	userMetadata                          map[string]string
 }
 
-- 
2.42.0

