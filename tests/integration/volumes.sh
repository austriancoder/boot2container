source ./tests/integration/base.sh

testVolumes() {
    qemu_params="$QEMU_NIC $QEMU_DISK -virtfs local,path=/tmp/qemufs,mount_tag=qemufs,security_model=passthrough"

    local qemufs_mount_point=/tmp/qemufs
    mkdir -p "$qemufs_mount_point"
    rm "$qemufs_mount_point/helloworld" 2> /dev/null

    reset_disk

    # Check the persistence of data in a volume between containers, and then that mirroring to a folder works
    kernel_cmdline='b2c.volume=myvol,mirror=/tmp/volume,push_on=container_end,fscrypt_key=LvOC5xFSrMxJymf2XOwHGPBj7x1mENBuBWxV1a10LBgV5LG29VTgCpjlR4Ng7j0e5vTPH2WlAcU0R/mPYlPttw== \
        b2c.filesystem="qemufs,src=qemufs,type=9p" \
        b2c.volume="qemufs,filesystem=qemufs" \
        b2c.cache_device=auto \
        b2c.run="-v myvol:/myvol -v qemufs:/qemufs docker.io/library/busybox:latest touch /myvol/result /qemufs/helloworld" \
        b2c.run="-v myvol:/myvol docker.io/library/busybox:latest [ -f /myvol/result ]" \
        b2c.run="-v /tmp/volume:/mnt_volume docker.io/library/busybox:latest [ -f /mnt_volume/result ]"'

    run_test

    assertContains "$stdout" "Execution is over, pipeline status: 0"
    assertTrue 'Failed to share a file from the guest to the host' "[ -r /tmp/qemufs/helloworld ]"

}
suite_addTest testVolumes
