reset_disk() {
    rm "$DISK_PATH" 2> /dev/null
}

run_test() {
    tmpfile=$(mktemp /tmp/qemu_exec.XXXXXX)

    workdir="/tmp/workdir"
    mkdir -p "$workdir"

    set -o pipefail
    eval "timeout 60 ./vm2c.py --workdir ${workdir}" | tee "$tmpfile"
    exit_code=$?
    set +o pipefail

    stdout=$(cat "$tmpfile")
    rm "$tmpfile"
}
